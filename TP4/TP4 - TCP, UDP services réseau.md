# I. First steps

**Top 5 applications utilisées :**

```
- Spotify
- Discord
- Prospect Mail
- Tryhackme
- Youtube
```

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

```
#Spotify :
IP et port serveur : 35.186.224.40:443 --> port local : 49570
#Discord :
IP et port serveur : 162.159.136.234:443 --> port local : 40472
#Prospect Mail :
IP et port serveur : 52.97.212.82:443 --> port local : 44044
#Tryhackme : 
IP et port serveur : 216.58.213.74:443 --> port local : 55554
#Youtube :
IP et port serveur : 91.68.245.15:443 --> port local : 53363
```

🌞 **Demandez l'avis à votre OS**

```
marty@martywork-ROGFlowX13:~$ ss -tpn | grep spotify                                    
ESTAB 0      0       10.33.17.218:49570 35.186.224.40:443    users:(("spotify",pid=20078,fd=32)) 
[...]
marty@martywork-ROGFlowX13:~$ ss -tpn | grep Discord
ESTAB 0      0       10.33.17.218:42690 162.159.136.234:443  users:(("Discord",pid=20618,fd=34))
[...]
marty@martywork-ROGFlowX13:~$ ss -tpn | grep prospect
ESTAB 0      0       10.33.17.218:56436   52.97.212.82:443  users:(("prospect-mail",pid=21202,fd=26))
#Tryhackme
marty@martywork-ROGFlowX13:~$ ss -tpn | grep chrome
ESTAB 73     0       10.33.17.218:58370  216.58.213.74:443  users:(("chrome",pid=19424,fd=66))
#Youtube
marty@martywork-ROGFlowX13:~$ ss -tpn | grep chrome
ESTAB      0      0       10.33.17.218:38506 142.250.74.246:443  users:(("chrome",pid=19424,fd=53))
```

# II. Mise en place

## 1. SSH

Connectez-vous en SSH à votre VM.
```
marty@martywork-ROGFlowX13:~$ ssh marty@192.168.56.101
[...]
[marty@localhost ~]$ 
```

🌞 **Examinez le trafic dans Wireshark**

- **déterminez si SSH utilise TCP ou UDP**
  - SSH utilise TCP puisque l'on a besoin d'une communication de paquets très fiable
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - voir capture `ssh.pcapng`

- **repérez le FIN FINACK à la fin d'une connexion**
  - voir capture `ssh.pcapng`

🌞 **Demandez aux OS**

- repérage, avec une commande adaptée, la connexion SSH depuis ma machine :
```
marty@martywork-ROGFlowX13:~$ sudo ss -tpn | grep ssh
ESTAB 0      0       192.168.56.1:39508 192.168.56.101:22   users:(("ssh",pid=5461,fd=3))   
```

- ET repérage la connexion SSH depuis ma VM :

```
[marty@localhost ~]$ sudo ss -tpn | grep ssh
ESTAB 0      0      192.168.56.101:22   192.168.56.1:39508 users:(("sshd",pid=1007,fd=4),("sshd",pid=1003,fd=4))
```

## 2. NFS

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

- Config NFS serveur :

```
[marty@localhost ~]$ dnf -y install nfs-utils
[marty@localhost ~]$ sudo nano /etc/idmapd.conf
[marty@localhost ~]$ cat /etc/idmapd.conf | head -n5 | tail -n1
Domain = srv.world
[marty@localhost ~]$ sudo nano /etc/exports
[marty@localhost ~]$ cat /etc/exports
/srv/nfsshare 192.168.56.0/24(rw,no_root_squash)
[marty@localhost ~]$ mkdir /srv/nfsshare
[marty@localhost ~]$ sudo systemctl enable --now rpcbind nfs-server
[marty@localhost ~]$ sudo firewall-cmd --add-service=nfs
  success
[marty@localhost ~]$ sudo firewall-cmd --runtime-to-permanent
  success
```

- Config NFS client :
```
[marty@localhost ~]$ dnf -y install nfs-utils
[marty@localhost ~]$ sudo nano /etc/idmapd.conf
[marty@localhost ~]$ cat /etc/idmapd.conf | head -n5 | tail -n1
Domain = srv.world
```

- partagez un dossier que vous avez créé au préalable dans `/srv`
- vérifiez que vous accédez à ce dossier avec l'autre machine : [le client NFS](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)


🌞 **Wireshark it !**

- une fois que c'est en place, utilisez `tcpdump` pour capturer du trafic NFS
- déterminez le port utilisé par le serveur

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion NFS sur le client et sur le serveur

```
# GNU/Linux
$ ss
```

🦈 **Et vous me remettez une capture de trafic NFS** la plus complète possible. J'ai pas dit que je voulais le plus de trames possible, mais juste, ce qu'il faut pour avoir un max d'infos sur le trafic

## 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs

- Requête DNS vers `gitlab.com` -> je me connecte au serveur DNS à l'IP `10.0.3.3:53`
- Voir capture `dns.pcapng`
