# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande
🌞 Affichez les infos des cartes réseau de votre PC

```
marty@martywork-ROGFlowX13:~$ ip addr
    link/ether 70:9c:d1:5a:d0:2f brd ff:ff:ff:ff:ff:ff
    inet 10.33.18.125/22 brd 10.33.19.255 scope global dynamic noprefixroute wlp6s0
```

🌞 Affichez votre gateway

```
marty@martywork-ROGFlowX13:~$ ip route
default via 10.33.19.254 dev wlp6s0 proto dhcp metric 600
```
  
### En graphique (GUI : Graphical User Interface)
🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

- (ubuntu KDE) Cliquer sur démarrer -> Taper "info center" -> Aller dans network


### Questions
🌞 à quoi sert la gateway dans le réseau d'YNOV ?

- La gateway YNOV sert à faire communiquer le réseau avec internet ou un autre réseau par exemple


## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  
🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP 

- De manière non persistente :
```
marty@martywork-ROGFlowX13:~$ sudo ifconfig wlp6s0 10.33.18.200/22
marty@martywork-ROGFlowX13:~$ ip addr
    inet 10.33.18.200/22 brd 10.33.19.255 scope global noprefixroute wlp6s0
```
🌞 Il est possible que vous perdiez l'accès internet.

- J'ai perdu l'accès internet parce que l'adresse ip choisie était probablement déjà utilisée, je suis donc ignoré

# II. Exploration locale en duo

## 3. Modification d'adresse IP
🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

**Choisir une IP (Part II faite par Lucas et Louis car pas de cable RJ45)**
1. Panneau de configuration\Réseau et Internet\Connexions réseau
2. Clique droit sur la carte wifi puis propriétés
3. Cliquer sur Protocole Internet version 4 (TCP/IPv4) puis propriétés
4. Dans Général modifier l'IP manuellement
```
Adresse IP : 10.33.18.85
Longueur du préfixe de sous-réseau : 255.255.252.0
Passerelle : 10.33.19.254

PS C:\Users\gympl> ipconfig
[...]
Carte Ethernet Ethernet :
[...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.102
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
```

**Test ping d'une machine à l'autre :**
```
PS C:\Users\gympl> ping 192.168.1.101

Envoi d’une requête 'Ping'  192.168.1.101 avec 32 octets de données :
Réponse de 192.168.1.101 : octets=32 temps=1 ms TTL=128
```
**Affichage de la table ARP :**
```
PS C:\Users\gympl> arp -a
Interface : 192.168.1.102 --- 0x6
  Adresse Internet      Adresse physique      Type
  192.168.1.101         98-28-a6-16-1f-c4     dynamique
  192.168.1.103         ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

## 4. Utilisation d'un des deux comme gateway
**Sur le PC qui où Internet  est désactivé :**
```
Modification de la passerelle par l'ip du pc ayant la connection à savoir :
192.168.1.102
```
**Sur le PC qui où Internet  est activé :**
```
Dans le panneau de configuration -> Réseau et Internet -> Etat de Wifi -> Propriétés de Wifi-> Partage -> activer le partage de wifi via Ethernet.
```
**Résolution de ping vers serveur google :**
```
 PS C:\Users\Lucas> ping 8.8.8.8 Envoi d’une requête 'Ping' 8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=113 
```
**Utilisation traceroute :**
```
PS C:\Users\Lucas> tracert google.com 
Détermination de l’itinéraire vers google.com [216.58.215.46] avec un maximum de 30 sauts :
1 <1 ms * <1 ms LAPTOP-2P72Q5OG [192.168.137.1]
```

## 5. Petit chat privé
**PC Serveur :**
```
PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888
```
**PC Client :**
```
PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888
```

**Discussion intéréssante :**
```
PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888
fpobs
gros fdp
téo c un boffon
```
**Allons plus loin dans le chat :**
```
PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888 192.168.137.1
```
Seul l'IP marquée dans la commande peut se connecter

## 6. Firewall

**Autorisation des pings :**
 
**Dans un powershell en mode admin :**
```
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
```


**PC 1 :**

![Screen PC 1](https://i.ibb.co/PhBd67Y/pc1.png)

**PC 2 :**

![Screen PC 2](https://i.ibb.co/NW3xx5f/PC2-PNG.png)

**Autorisation Netcat sur le port 8888 :**
```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name= "Open Port 8888" dir=in action=allow protocol=TCP localport=8888
Ok.
```
  
# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP
🌞Exploration du DHCP, depuis votre PC
- Adresse IP du serveur DHCP 
```
marty@martywork-ROGFlowX13:/var/lib/dhcp$ nmcli con show WiFi@YNOV | grep -i dhcp
DHCP4.OPTION[2]:                        dhcp_server_identifier = 10.33.19.254
``` 

- Date d'expiration bail DHCP
```
marty@martywork-ROGFlowX13:/var/lib/dhcp$ nmcli con show WiFi@YNOV | grep -i dhcp
DHCP4.OPTION[4]:                        expiry = 1664452692
```

## 2. DNS
🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```
marty@martywork-ROGFlowX13:~$ nmcli
DNS configuration:
        servers: 8.8.8.8 8.8.4.4 1.1.1.1
        interface: wlp6s0
```

🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

- **faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")**

IP serveur google.com :
```
marty@martywork-ROGFlowX13:~$ dig google.com +short
216.58.214.174
```
**Requête envoyée au serveur google**

IP serveur ynov.com :
```
marty@martywork-ROGFlowX13:~$ dig ynov.com +short
104.26.10.233
104.26.11.233
172.67.74.226
```
**Ces requêtes ont été envoyées aux datacenters qu'utilise ynov**


- **faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")**

IP `78.74.21.21` :
```
marty@martywork-ROGFlowX13:~$ dig -x 78.74.21.21 +short
host-78-74-21-21.homerun.telia.com.
```
**Il s'agirait d'un opérateur suédois**

IP `92.146.54.88` :
```
marty@martywork-ROGFlowX13:~$ dig -x 92.146.54.88 +short
marty@martywork-ROGFlowX13:~$ 
```
**Il n'y a pas de sortie, cette IP ne doit pas être utilisée**


# IV. Wireshark
🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

**ping entre vous et la passerelle :**
```
5	0.192449731	10.33.18.125	10.33.19.254	ICMP	98	Echo (ping) request  id=0x0006, seq=9/2304, ttl=64 (no response found!)
```
(je n'ais pas fais la partie II je n'ai donc pas autorisé le ping)

**un netcat entre vous et votre mate, branché en RJ45 :**
(fais sur les PC de Lucas et Louis)
```
42    12.742014    192.168.137.2    192.168.137.1    TCP    64    8888 → 33377 [PSH, ACK] Seq=1 Ack=5 Win=2097920 Len=10
```
**une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question :**
```
70	50.668970314	10.33.18.125	8.8.4.4	DNS	100	Standard query 0x288f A clientservices.googleapis.com OPT
```



