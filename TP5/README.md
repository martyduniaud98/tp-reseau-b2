# Sujet Réseau et Infra
 **Projet fait avec TOULEMON Louis**
 
# Sommaire

- [Sujet Réseau et Infra](#sujet-réseau-et-infra)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Dumb switch](#i-dumb-switch)
  - [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Topologie 2](#1-topologie-2)
  - [2. Adressage topologie 2](#2-adressage-topologie-2)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [1. Topologie 3](#1-topologie-3)
  - [2. Adressage topologie 3](#2-adressage-topologie-3)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [1. Topologie 4](#1-topologie-4)
  - [2. Adressage topologie 4](#2-adressage-topologie-4)
  - [3. Setup topologie 4](#3-setup-topologie-4)
- [V. Add a building](#v-add-a-building)
  - [1. Topologie 5](#1-topologie-5)
  - [2. Adressage topologie 5](#2-adressage-topologie-5)
  - [3. Setup topologie 5](#3-setup-topologie-5)

# Prérequis

## Checklist VM Linux

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel
- [x] résolution de nom
  - vers internet, quand vous aurez le routeur en place

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Dumb switch

## 1. Topologie 1

![Topologie 1](./pics/topo1.png)

## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.5.1.1/24` |
| `pc2` | `10.5.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définition des IP static PC1 et PC2 :
```
PC1> ip 10.5.1.1/24
Checking for duplicate address...
PC1 : 10.5.1.1 255.255.255.0

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.5.1.1/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 10002
RHOST:PORT  : 127.0.0.1:10003
MTU:        : 1500

PC2> ip 10.5.1.2/24
Checking for duplicate address...
PC1 : 10.5.1.2 255.255.255.0

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.5.1.2/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 10004
RHOST:PORT  : 127.0.0.1:10005
MTU:        : 1500
```

- `ping` un VPCS depuis l'autre :
```
PC2> ping 10.5.1.1
84 bytes from 10.5.1.1 icmp_seq=1 ttl=64 time=0.625 ms
84 bytes from 10.5.1.1 icmp_seq=2 ttl=64 time=0.665 ms
```


# II. VLAN

## 1. Topologie 2

![Topologie 2](./pics/topo2.png)

## 2. Adressage topologie 2

| Node  | IP             | VLAN |
|-------|----------------|------|
| `pc1` | `10.5.10.1/24` | 10   |
| `pc2` | `10.5.10.2/24` | 10   |
| `pc3` | `10.5.10.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- Définition IP static des 3 PC :
```
PC1> ip 10.5.10.1/24
PC2> ip 10.5.10.2/24
PC3> ip 10.5.10.3/24
```

- vérifiez avec des `ping` que tout le monde se ping
```
PC1> ping 10.5.10.2
84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=0.245 ms
PC1> ping 10.5.10.3
84 bytes from 10.5.10.3 icmp_seq=2 ttl=64 time=0.398 ms
```

🌞 **Configuration des VLANs**

- référez-vous [à la section VLAN du mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
```
sw1# conf t
sw1(config)# vlan 10
sw1(config-vlan)# name guests
sw1(config-vlan)# exit

sw1(config)# vlan 20
sw1(config-vlan)# name admins
sw1(config-vlan)# exit
```

- ajout des ports du switches dans le bon VLAN :
```
sw1(config)# interface Ethernet0/0
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 10
sw1(config-if)# exit
sw1(config)# interface Ethernet0/1
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 10
sw1(config-if)# exit
sw1(config)# interface Ethernet1/0
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 20
sw1(config-if)# exit
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping :
```
PC1> ping 10.5.10.2
84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=0.625 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=0.665 ms
```

- `pc3` ne ping plus personne
```
PC3> ping 10.5.10.1
host (10.5.10.1) not reachable
PC3> ping 10.5.10.2
host (10.5.10.2) not reachable
```

# III. Routing

## 1. Topologie 3

![Topologie 3](./pics/topo3.png)

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `guests`  | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

> **Question de bonne pratique** : on fait apparaître le numéro du VLAN dans l'adresse du réseau concerné. En effet, souvent, à un VLAN donné est associé un réseau donné. Par exemple le VLAN **20** correspond au réseau 10.5.**20**.0/24.

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`        | `admins`         | `servers`        |
|--------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`  | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`  | `10.5.10.2/24`   | x                | x                |
| `adm1.admins.tp5`  | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5` | x                | x                | `10.5.30.1/24`   |
| `r1`               | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp5`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définir les IPs statiques sur toutes les machines **sauf le *routeur*** :
```
PC1> ip 10.5.10.1/24
PC2> ip 10.5.10.2/24
adm1> ip 10.5.20.1/24
[marty@web1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.5.30.1
NETMASK=255.255.255.0
```

🌞 **Configuration des VLANs**

- déclaration des VLANs sur le switch `sw1` :
```
sw1# conf t
sw1(config)# vlan 10
sw1(config-vlan)# name guests
sw1(config-vlan)# exit

sw1(config)# vlan 20
sw1(config-vlan)# name admins
sw1(config-vlan)# exit

sw1(config)# vlan 30
sw1(config-vlan)# name servers
sw1(config-vlan)# exit
```

- ajout des ports du switch dans le bon VLAN :
```
#interface PC1
sw1(config)# interface Ethernet0/0
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 10
sw1(config-if)# exit
#interface PC2
sw1(config)# interface Ethernet0/1
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 10
sw1(config-if)# exit
#interface adm1
sw1(config)# interface Ethernet1/0
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 20
sw1(config-if)# exit
#interface web1
sw1(config)# interface Ethernet2/0
sw1(config-if)# switchport mode access
sw1(config-if)# switchport access vlan 30
sw1(config-if)# exit
sw1(config)# no shut

```
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* :
```
sw1(config)# interface Ethernet3/3
sw1(config-if)# switchport trunk encapsulation dot1q
sw1(config-if)# switchport mode trunk
sw1(config-if)# switchport trunk allowed vlan add 10,20,30
sw1(config-if)# exit
sw1(config)# exit
```

---

➜ **Pour le *routeur***

🌞 **Config du *routeur***

```
# conf t
(config)# interface fastEthernet 0/0.1
R1(config-subif)# encapsulation dot1Q 10
R1(config-subif)# ip addr 10.5.10.254 255.255.255.0 
R1(config-subif)# exit
(config)# interface fastEthernet 0/0.2
R1(config-subif)# encapsulation dot1Q 20
R1(config-subif)# ip addr 10.5.20.254 255.255.255.0 
R1(config-subif)# exit
(config)# interface fastEthernet 0/0.3
R1(config-subif)# encapsulation dot1Q 30
R1(config-subif)# ip addr 10.5.30.254 255.255.255.0 
R1(config-subif)# exit
(config)# no shut
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau : 
```
PC1> ping 10.5.10.254
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=7.177 ms
PC2> ping 10.5.10.254
84 bytes from 10.5.10.254 icmp_seq=1 ttl=255 time=7.272 ms
adm1> ping 10.5.20.254
84 bytes from 10.5.20.254 icmp_seq=1 ttl=255 time=7.272 ms
[marty@web1 ~]$ ping 10.5.30.254
84 bytes from 10.5.30.254 icmp_seq=1 ttl=255 time=7.222 ms
```

- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux :
```
PC1> ip 10.5.10.1/24 10.5.10.254
PC2> ip 10.5.10.2/24 10.5.10.254
adm1> ip 10.5.20.1/24 10.5.20.254
[marty@web1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3 | grep GATEWAY
GATEWAY=10.5.30.254

PC1> ping 10.5.10.2
84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=0.272 ms
PC1> ping 10.5.20.1
84 bytes from 10.5.20.1 icmp_seq=1 ttl=63 time=31.123 ms
PC1> ping 10.5.30.1
84 bytes from 10.5.30.1 icmp_seq=1 ttl=63 time=16.252 ms
```


# IV. NAT

## 1. Topologie 4

![Topologie 3](./pics/topo4.png)

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `guests`  | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`        | `admins`         | `servers`        |
|--------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`  | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`  | `10.5.10.2/24`   | x                | x                |
| `adm1.admins.tp5`  | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5` | x                | x                | `10.5.30.1/24`   |
| `r1`               | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP :
```
R1(config)#interface FastEthernet0/1
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit

R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
[...]   
FastEthernet0/1            10.0.3.16       YES DHCP   up                    up      
[...]
```

- vous devriez pouvoir `ping 1.1.1.1`
```
R1#ping 1.1.1.1
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 20/28/36 ms
```


🌞 **Configurez le NAT**
```
R1#conf t
R1(config)#interface fastEthernet0/1
R1(config-if)#ip nat outside
*Mar  1 00:41:31.611: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface fastEthernet0/0
R1(config-if)#ip nat inside
R1(config-if)#exit

R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet0/1 overload
```

🌞 **Test**

- configurez l'utilisation d'un DNS
  - sur les VPCS :
  ```
  PC1> ip dns 1.1.1.1
  PC2> ip dns 1.1.1.1
  adm1> ip dns 1.1.1.1
  ```
  - sur la machine Linux : 
  ```
  [marty@web1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3 | grep DNS
  DNS1=1.1.1.1
  ```

- vérifiez un `ping` vers un nom de domaine :
```
PC1> ping gitlab.com
84 bytes from 172.65.251.78 icmp_seq=1 ttl=61 time=39.645 ms
PC2> ping gitlab.com
84 bytes from 172.65.251.78 icmp_seq=1 ttl=61 time=39.645 ms
adm1> ping gitlab.com
84 bytes from 172.65.251.78 icmp_seq=1 ttl=61 time=39.645 ms
[marty@web ~]$ ping gitlab.com
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=61 time=49.5 ms
```

# V. Add a building

On a acheté un nouveau bâtiment, faut tirer et configurer un nouveau switch jusque là-bas.

On va en profiter pour setup un serveur DHCP pour les clients qui s'y trouvent.

## 1. Topologie 5

![Topo 5](./pics/topo5.png)

## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `clients` | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`        | `admins`         | `servers`        |
|---------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`   | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`   | `10.5.10.2/24`   | x                | x                |
| `pc3.clients.tp5`   | DHCP             | x                | x                |
| `pc4.clients.tp5`   | DHCP             | x                | x                |
| `pc5.clients.tp5`   | DHCP             | x                | x                |
| `dhcp1.clients.tp5` | `10.5.10.253/24` | x                | x                |
| `adm1.admins.tp5`   | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5`  | x                | x                | `10.5.30.1/24`   |
| `r1`                | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
    ```
    R1#show running-config
    interface FastEthernet0/0.1
    encapsulation dot1Q 10
    ip address 10.5.10.254 255.255.255.0
    !
    interface FastEthernet0/0.2
    encapsulation dot1Q 20
    ip address 10.5.20.254 255.255.255.0
    !
    interface FastEthernet0/0.3
    encapsulation dot1Q 30
    ip address 10.5.30.254 255.255.255.0
    !
    interface FastEthernet0/1
    ip address dhcp
    ip nat outside
    ip virtual-reassembly
    duplex auto
    speed auto
    !
    interface FastEthernet1/0
    no ip address
    shutdown
    duplex auto
    speed auto
    !
    interface FastEthernet2/0
    no ip address
    shutdown 
    duplex auto
    speed auto
    !
    ip forward-protocol nd
    !
    !
    no ip http server
    no ip http secure-server
    ip nat inside source list 1 interface FastEthernet0/1 overload
    !
    access-list 1 permit any
    ```
  - Switch 1
    ```
    sw1#show running-config
    interface Ethernet0/0
    !
    interface Ethernet0/1
    !
    interface Ethernet0/2
    !
    interface Ethernet0/3
    !
    interface Ethernet1/0
    !
    interface Ethernet1/1
    !
    interface Ethernet1/2
    !
    interface Ethernet1/3
    !         
    interface Ethernet2/0
    !
    interface Ethernet2/1
    !
    interface Ethernet2/2
    !
    interface Ethernet2/3
    !
    interface Ethernet3/0
    switchport trunk encapsulation dot1q
    switchport mode trunk
    !
    interface Ethernet3/1
    !
    interface Ethernet3/2
    !
    interface Ethernet3/3
    switchport trunk encapsulation dot1q
    switchport mode trunk
    ```
  - Switch 2 :
    ```
    sw2#show running-config
    interface Ethernet0/0
    switchport access vlan 10
    switchport mode access
    !
    interface Ethernet0/1
    switchport access vlan 10
    switchport mode access
    !
    interface Ethernet0/2
    !
    interface Ethernet0/3
    !
    interface Ethernet1/0
    switchport access vlan 20
    switchport mode access
    !
    interface Ethernet1/1
    !
    interface Ethernet1/2
    !
    interface Ethernet1/3
    !
    interface Ethernet2/0
    switchport access vlan 30
    switchport mode access
    !
    interface Ethernet2/1
    !
    interface Ethernet2/2
    !
    interface Ethernet2/3
    !
    interface Ethernet3/0
    !
    interface Ethernet3/1
    !
    interface Ethernet3/2
    !         
    interface Ethernet3/3
    switchport trunk encapsulation dot1q
    switchport mode trunk
    ```

  - Switch 3 : 
    ```
    sw3#show running-config
    interface Ethernet0/0
    switchport access vlan 10
    switchport mode access
    !
    interface Ethernet0/1
    switchport access vlan 10
    switchport mode access
    !
    interface Ethernet0/2
    switchport access vlan 10
    switchport mode access
    !
    interface Ethernet0/3
    switchport access vlan 10
    switchport mode access
    !         
    interface Ethernet1/0
    switchport trunk encapsulation dot1q
    switchport mode trunk
    !
    interface Ethernet1/1
    !
    interface Ethernet1/2
    !
    interface Ethernet1/3
    !
    interface Ethernet2/0
    !
    interface Ethernet2/1
    !
    interface Ethernet2/2
    !
    interface Ethernet2/3
    !
    interface Ethernet3/0
    !
    interface Ethernet3/1
    !
    interface Ethernet3/2
    !
    interface Ethernet3/3
    !
    interface Vlan1
    no ip address
    ```

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

- il doit distribuer des IPs aux clients dans le réseau `guests` qui sont branchés au même switch que lui
- sans aucune action manuelle, les clients doivent...
  - avoir une IP dans le réseau `guests`
  - avoir un accès au réseau `servers`
  - avoir un accès WAN
  - avoir de la résolution DNS
  ```
  [marty@dhcp1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
  default-lease-time 900;
  max-lease-time 10000;

  authoritative;

  subnet 10.5.10.0 netmask 255.255.255.0 {
  range dynamic-bootp 10.5.10.3 10.5.10.252;
  option broadcast-address 10.5.10.255;
  option routers 10.5.10.254;
  option domain-name-servers 1.1.1.1;
  }
  ```

🌞  **Vérification**

- un client récupère une IP en DHCP
- il peut ping le serveur Web
```
PC3> ping 10.5.30.1 
84 bytes from 10.5.30.1 icmp_seq=1 ttl=63 time=17.559 ms
```

- il peut ping `8.8.8.8`
```
PC3> ping 8.8.8.8
84 bytes from 8.8.8.8 icmp_seq=1 ttl=61 time=80.049 ms
```

- il peut ping `google.com`
```
PC3> ping google.com
84 bytes from 142.250.179.78 icmp_seq=1 ttl=61 time=73.783 ms
```
